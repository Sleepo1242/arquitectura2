/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Modelo.persistencia.Marca;
import Modelo1.*;
import Vista.*;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Emilio
 */
public class ControladorMarca implements ActionListener{
    
    
    Vista_Marca marca;
    
    public ControladorMarca(Vista_Marca marca) {
        this.marca = marca;
        
        this.marca.botón_ag.addActionListener(this);
        marca.setVisible(true);
        marca.setLocationRelativeTo(null);
    }
    
    public void inicializarCrud(){
        
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == marca.botón_ag) {
            Marca marc = new Marca(Integer.parseInt(marca.text1.getText()),marca.text2.getText());
            if (ConsultaCRUDMarca.crearMarca(marc)) {
                JOptionPane.showMessageDialog(null, "Registro de Marca Exitoso");
            }
            
        }
        
    }
    
}
